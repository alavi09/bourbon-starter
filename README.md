# bourbon-starter

Simple bourbon+neat project starter.

1. After clone, run 'npm install'.
2. Install bourbon and neat using bower, 'bower install --save bourbon' and 'bower install --save neat'.
3. Run grunt to watch scss files and update changes.
