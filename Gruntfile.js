/*
Watch for changes in sass and less files and auto compile.
Directory:
|_root
   |-less
   |-sass

*/

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			task: {
				expand: true,
				cwd: 'less',
				src: ['*.less'],
				dest: 'less',
				ext: '.css'
			}
		},
		sass: {
    dist: {//watch /sass folder(expand: true) and outputcss file to same folder
      files: [{
        expand: true,
        cwd: 'css',
        src: ['*.scss'],
        dest: 'css',
        ext: '.css'
      }]
    }
		},
		watch: {
			options: {livereload: true},
			task: {
				files: ['**/*.less', '**/*.scss', '**/*.html'],
				tasks: ['less', 'sass:dist', 'connect:server']
			}
		},
		connect: {
	    server: {
				options: {
								'port': 9000,
								'protocol': 'http',
								'hostname': '127.0.0.1',
								'base': '.',
								'keepalive': true
							}
	    }
	  },
		concurrent: {
		target: {
				tasks: ['connect', 'watch']
		}
}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-concurrent');

	grunt.registerTask('default', ['concurrent']);
};
